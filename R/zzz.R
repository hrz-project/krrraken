.onLoad <- function(libname, pkgname) {
  .inpath <- function(x) {
    fnd <- Sys.which(x) != ""
    if (fnd) {
      packageStartupMessage("found ", x)
    } else {
      stop(x, "binary not in path")
    }
  }
  .inpath("kraken2")
  .inpath("kraken2-build")
}
